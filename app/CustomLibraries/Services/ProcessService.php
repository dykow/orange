<?php


namespace App\CustomLibraries\Services;


use App\Device;
use App\Flag;
use DateTime;
use RuntimeException;

class ProcessService extends BaseService
{
    private $deviceService;
    private $flagService;

    public function __construct(DeviceService $deviceService,
                                FlagService $flagService)
    {
        $this->deviceService = $deviceService;
        $this->flagService = $flagService;
    }

    /**
     * runs the processing of given device with given flag
     *
     * @param Device $device
     * @param Flag $flag
     * @return string
     */
    public function process(Device $device,
                            Flag $flag) : string
    {
        if ($this->canProcess($device, $flag)) {
            try {
                $this->attemptToSaveStage($device, $flag);
            }
            catch (RuntimeException $e) {
                ////
                throw new RuntimeException($e->getMessage());
            }

            return "Stage updated successfully!";
        }

        throw  new  RuntimeException("Malformed request!");
    }

    private function canProcess(Device $device,
                                Flag $flag) : bool
    {
        return $this->deviceService->deviceIsValid($device)
            && $this->flagService->flagNameIsValid($flag);
    }

    private function attemptToSaveStage(Device $device,
                                        Flag $flag) : void
    {
        // TODO move extracting attributes to separate method
        $serialNumber = $device->getAttributes();
        $serialNumber = $serialNumber['serial_number'];
        $flagName = $flag->getAttributes();
        $flagName=  $flagName['name'];

        // TODO separate to a method
        if ($this->deviceService->exists($serialNumber)) {
            $deviceCurrentState = $this->deviceService->getMostRecentFlag($device);
            $deviceCurrentState = $deviceCurrentState['name'];
        } else {
            // TODO set something like "device is not in any state" instead of empty
            $deviceCurrentState = '';
        }

        $nextCorrectStates = $this->flagService->nextAvailableStates($deviceCurrentState);
        if (in_array($flagName, $nextCorrectStates, true)) {
            $this->flagService->saveFlag($flag);
            $this->deviceService->updateIfExistsOrCreate($device);
        } else {
            throw new RuntimeException('Could not update stage! Flag not permitted!');
        }
    }
}
