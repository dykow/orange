<?php


namespace App\CustomLibraries\Services;

use App\Flag;

class FlagService extends BaseService
{
    private const VALID_FLAGS = [
        'dekompletacja',
        'testowanie_uszkodzony',
        'pakowanie_uszkodzony',
        'testowanie_sprawny',
        'czyszczenie',
        'wymiana_obudowy',
        'pakowanie'
    ];

    /**
     * validates flag name string
     *
     * @param Flag $flag
     * @return bool
     */
    public function flagNameIsValid(Flag $flag) : bool
    {
        $flagName = $flag->getAttributes();
        $flagName = $flagName['name'];
        return in_array($flagName, self::VALID_FLAGS, true);
    }

    /**
     * Provides next correct flag based on current (the most recent) flag
     *
     * @param string $currentFlag
     * @return array|string[]
     */
    public function nextAvailableStates(string $currentFlag): array
    {
        switch ($currentFlag)
        {
            case '':
                return ['dekompletacja'];
                break;
            case 'dekompletacja':
                return [
                    'testowanie_uszkodzony',
                    'testowanie_sprawny'
                ];
                break;
            case 'testowanie_uszkodzony':
                return ['pakowanie_uszkodzony'];
                break;
            case 'testowanie_sprawny':
                return [
                    'czyszczenie',
                    'wymiana_obudowy'
                ];
                break;
            case 'pakowanie':
            case 'pakowanie_uszkodzony':
                return [];
                break;
            case 'czyszczenie':
            case 'wymiana_obudowy':
                return ['pakowanie'];
                break;
            default:
                return [];
        }
    }

    /**
     * Service function for saving Device into database;
     * more logic can be added
     *
     * @param Flag $flag
     */
    public function saveFlag(Flag $flag): void
    {
        $flag->save();
    }
}
