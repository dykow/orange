<?php


namespace App\CustomLibraries\Services;


use App\Device;
use App\Flag;
use App;
use DateTime;

class DeviceService extends BaseService
{
    /**
     * Checks if devices has valid props
     *
     * @param Device $device
     * @return bool
     */
    public function deviceIsValid(Device $device) : bool
    {
        $serialNumber = $device->getAttributes();
        $serialNumber = $serialNumber['serial_number'];
        return !empty($serialNumber);
    }

    /**
     * Returns the most recent flag
     *
     * @param Device $device
     * @return App\Flag
     */
    public function getMostRecentFlag(Device $device) : Flag
    {
        $serialNumber = $device->getAttributes();
        $serialNumber = $serialNumber['serial_number'];
        $flags =  Device::find($serialNumber)->flags->sortDesc();
        return $flags->first();
    }

    /**
     * Decides whether only update modification time
     * or crate save new device into databse
     *
     * @param Device $device
     */
    public function updateIfExistsOrCreate(Device $device) : void
    {
        $serialNumber = $device->getAttributes();
        $serialNumber = $serialNumber['serial_number'];
        if ($this->exists($serialNumber)) {
            $this->updateModificationTime($serialNumber);
        } else {
            $this->saveDevice($device);
        }
    }

    /**
     * checks if device with given serial_number is present in DB
     *
     * @param string $serialNumber
     * @return bool
     */
    public function exists(string $serialNumber) : bool
    {
        return Device::where('serial_number', $serialNumber)->exists();
    }

    /**
     * Service function for modifying updated_at timestamp
     *
     * @param string $serialNumber
     */
    public function updateModificationTime(string $serialNumber) : void
    {
        $recentTime = $this->getMostRecentFlagTime($serialNumber);

        $device = Device::find($serialNumber);
        $device->updated_at = $recentTime;
        $device->save();
    }

    /**
     * gets updated_at timestamp of the most recent flag for given serial_number
     *
     * @param string $serialNumber
     * @return DateTime
     * @throws \Exception
     */
    public function getMostRecentFlagTime(string $serialNumber) : DateTime
    {
        $flags = Device::find($serialNumber)->flags->sortDesc();
        $recentFlagTime = $flags->first();
        $recentFlagTime = $recentFlagTime->getAttributes();
        return new DateTime($recentFlagTime['created_at']);
    }

    /**
     * Service function for saving Device into database;
     * more logic can be added
     *
     * @param Device $device
     */
    public function saveDevice(Device $device) : void
    {
        $device->save();
    }
}
