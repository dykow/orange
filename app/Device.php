<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    /*
     * The primary key of devices table
     *
     * @var string
     */
    protected $primaryKey = 'serial_number';

    /**
     * Dont increment key
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * define key type
     *
     * @var string
     */
    protected $keyType = 'string';

    public function flags()
    {
        return $this->hasMany('App\Flag', 'device_serial_number', 'serial_number');
    }
}
