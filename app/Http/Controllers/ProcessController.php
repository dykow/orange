<?php


namespace App\Http\Controllers;

use App\CustomLibraries\Services\ProcessService;
use App\Device;
use App\Flag;
use Illuminate\Http\Request;
use RuntimeException;

class ProcessController extends Controller
{
    public function store(Request $request, ProcessService $processService)
    {
        $device = $this->serializeRequestToDevice($request);
        $flag = $this->serializeRequestToFlag($request);

        try {
            $result =  $processService->process($device, $flag);
            return response()->json($result, 200);
        } catch (RuntimeException $e) {
            $result = $e->getMessage();
            return response()->json($result, 400);
        }
    }

    private function serializeRequestToDevice(Request $request) : Device
    {
        $device = new Device();
        if($request->exists('serial_number')) {
            $device->serial_number = $request->get('serial_number');
        }

        return $device;
    }

    private function serializeRequestToFlag(Request $request) : Flag
    {
        $flag = new Flag();
        if($request->exists('serial_number')) {
            $flag->device_serial_number = $request->get('serial_number');
        }
        if($request->exists('flag_name')) {
            $flag->name = $request->get('flag_name');
        }
        $flag->ip_address = $request->ip();

        return $flag;
    }
}
