*API for managing stages of device*

In order to deploy you need:
1. install composer, then laravel https://laravel.com/docs/7.x
2. create database of your choice, MySQL database adviced. Install MySQL using Docker:
- `docker run --name orange_mysql -e MYSQL_DATABASE=orange -e  MYSQL_ROOT_PASSWORD=password -d mysql`
-  to get ip of the container `docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' orange_mysql`
3. edit name of `.env.example` to `.env`
4. in `.env` set `DB_CONNECTION`, `DB_HOST`, `DB_PORT`, `DB_DATABASE`, `DB_USERNAME` `DB_PASSWORD`
5. install dependencies `composer install`
6. generate app key `php artisan key:generate`
7. migrate the database with `php artisan migrate:fresh`
8. run the app from root directory of the app `php artisan serve`

Valid endpoint:
`{host}/api/devices`

Required http params:
- `serial_number`
- `flag_nam`

Example POST request:
`http://127.0.0.1:8000/api/devices?serial_number=sn11&flag_name=dekompletacja`

Valid flag names:
-     dekompletacja
      testowanie_uszkodzony
      pakowanie_uszkodzony
      testowanie_sprawny
      czyszczenie
      wymiana_obudowy
      pakowanie
